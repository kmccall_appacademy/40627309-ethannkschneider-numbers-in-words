class Fixnum

  private
  def number_words
    number_words = {
      0 => "zero", 1 => "one", 2 => "two", 3 => "three", 4 => "four",
      5 => "five", 6 => "six", 7 => "seven", 8 => "eight", 9 => "nine",
      10 => "ten", 11 => "eleven", 12 => "twelve", 13 => "thirteen",
      14 => "fourteen", 15 => "fifteen", 16 => "sixteen",
      17 => "seventeen", 18 => "eighteen", 19 => "nineteen",
      20 => "twenty", 30 => "thirty", 40 => "forty",
      50 => "fifty", 60 => "sixty", 70 => "seventy", 80 => "eighty",
      90 => "ninety", 100 => "hundred", 1000 => "thousand",
      1_000_000 => "million", 1_000_000_000 => "billion",
      1_000_000_000_000 => "trillion"
    }

    number_words
  end

  private
  def single_digit(num = nil)

    num = self unless num
    number_words[num]

  end

  private
  def double_digits(num = nil)
    num = self unless num
    digits = num.to_s.split("").map { |x| x.to_i }

    if num.between?(10, 20)
      return number_words[num]
    elsif num % 10 == 0
      return number_words[num]
    else
      return [number_words[digits[0] * 10], single_digit(digits[-1])].join(" ")
    end
  end

  private
  def three_digits(num = nil)
    num = self unless num
    digits = num.to_s.split("").map { |x| x.to_i }
    if num == 100
      return "one hundred"
    elsif num % 100 == 0
      return [number_words[digits[0]], "hundred"].join(" ")
    elsif digits[1] != 0
      return [number_words[digits[0]], "hundred", double_digits(num % 100)].join(" ")
    else
      return [number_words[digits[0]], "hundred", single_digit(num % 10)].join(" ")
    end
  end

  private
  def four_digits(num = nil)
    num = self unless num
    digits = num.to_s.split("").map { |x| x.to_i }
    if num % 1000 == 0
      return [number_words[digits[0]], "thousand"].join(" ")
    elsif digits[1] != 0
      return [number_words[digits[0]], "thousand", three_digits(num % 1000)].join(" ")
    elsif digits[2] != 0
      return [number_words[digits[0]], "thousand", double_digits(num % 100)].join(" ")
    else digits[3] != 0
      return [number_words[digits[0]], "thousand", single_digit(num % 10)].join(" ")
    end
  end

  private
  def five_digits(num = nil)
    num = self unless num
    digits = num.to_s.split("").map { |x| x.to_i }
    first_two_digits = [digits[0], digits[1]].join.to_i

    if num % 10_000 == 0
      return [double_digits(first_two_digits), "thousand"].join(" ")
    elsif digits[1] != 0
      return [number_words[digits[0] * 10], four_digits(num % 10_000)].join(" ")
    elsif digits[2] != 0
      return [double_digits(first_two_digits), "thousand", three_digits(num % 1000)].join(" ")
    elsif digits[3] != 0
      return [double_digits(first_two_digits), "thousand", double_digits(num % 100)].join(" ")
    else
      return [double_digits(first_two_digits), "thousand", single_digit(num % 10)].join(" ")
    end
  end

  private
  def six_digits(num = nil)
    num = self unless num
    digits = num.to_s.split("").map { |x| x.to_i }

    if num % 100_000 == 0
      return [digits[0], "hundred thousand"].join(" ")
    elsif digits[1] != 0
      return [number_words[digits[0]], "hundred", five_digits(num % 100_000)].join(" ")
    elsif digits[2] != 0
      return [number_words[digits[0]], "hundred", four_digits(num % 10_000)].join(" ")
    elsif digits[3] != 0
      return [number_words[digits[0]], "hundred thousand", three_digits(num % 1_000)].join(" ")
    elsif digits[4] != 0
      return [number_words[digits[0]], "hundred thousand", double_digits(num % 100)].join(" ")
    elsif digits[5] != 0
      return [number_words[digits[0]], "hundred thousand", single_digit(num % 10)].join(" ")
    end
  end

  private
  def seven_digits(num = nil)
    num = self unless num
    digits = num.to_s.split("").map { |x| x.to_i }

    if num % 1_000_000 == 0
      return [number_words[digits[0]], "million"].join(" ")
    elsif digits[1] != 0
      return [number_words[digits[0]], "million", six_digits(num % 1_000_000)].join(" ")
    elsif digits[2] != 0
      return [number_words[digits[0]], "million", five_digits(num % 100_000)].join(" ")
    elsif digits[3] != 0
      return [number_words[digits[0]], "million", four_digits(num % 10_000)].join(" ")
    elsif digits[4] != 0
      return [number_words[digits[0]], "million", three_digits(num % 1000)].join(" ")
    elsif digits[5] != 0
      return [number_words[digits[0]], "million", double_digits(num % 100)].join(" ")
    else
      return [number_words[digits[0]], "million", single_digit(num % 10)].join(" ")
    end
  end

  private
  def eight_digits(num = nil)
    num = self unless num
    digits = num.to_s.split("").map { |x| x.to_i }
    first_two_digits = [digits[0], digits[1]].join.to_i

    if num % 10_000_000 == 0
      return [number_words[digits[0] * 10], "million"].join(" ")
    elsif digits[1] != 0
      return [number_words[digits[0] * 10], seven_digits(num % 10_000_000)].join(" ")
    elsif digits[2] != 0
      return [number_words[first_two_digits], "million", six_digits(num % 1_000_000)].join(" ")
    elsif digits[3] != 0
      return [number_words[first_two_digits], "million", five_digits(num % 100_000)].join(" ")
    elsif digits[4] != 0
      return [number_words[first_two_digits], "million", four_digits(num % 10_000)].join(" ")
    elsif digits[5] != 0
      return [number_words[first_two_digits], "million", three_digits(num % 1000)].join(" ")
    elsif digits[6] != 0
      return [number_words[first_two_digits], "million", double_digits(num % 100)].join(" ")
    else
      return [number_words[first_two_digits], "million", single_digit(num % 10)].join(" ")
    end
  end

  private
  def nine_digits(num = nil)
    num = self unless num
    digits = num.to_s.split("").map { |x| x.to_i }

    if num % 100_000_000 == 0
      return [number_words[digits[0]], "hundred million"].join(" ")
    elsif digits[1] != 0
      return [number_words[digits[0]], "hundred", eight_digits(num % 100_000_000)].join(" ")
    elsif digits[2] != 0
      return [number_words[digits[0]], "hundred", seven_digits(num % 10_000_000)].join(" ")
    elsif digits[3] != 0
      return [number_words[digits[0]], "hundred million", six_digits(num % 1_000_000)].join(" ")
    elsif digits[4] != 0
      return [number_words[digits[0]], "hundred million", five_digits(num % 100_000)].join(" ")
    elsif digits[5] != 0
      return [number_words[digits[0]], "hundred million", four_digits(num % 10_000)].join(" ")
    elsif digits[6] != 0
      return [number_words[digits[0]], "hundred million", three_digits(num % 1_000)].join(" ")
    elsif digits[7] != 0
      return [number_words[digits[0]], "hundred million", double_digits(num % 100)].join(" ")
    else
      return [number_words[digits[0]], "hundred million", single_digit(num % 10)].join(" ")
    end
  end

  private
  def ten_digits(num = nil)
    num = self unless num
    digits = num.to_s.split("").map { |x| x.to_i }

    if num % 1_000_000_000 == 0
      return [number_words[digits[0]], "billion"].join(" ")
    elsif digits[1] != 0
      return [number_words[digits[0]], "billion", nine_digits(num % 1_000_000_000)].join(" ")
    elsif digits[2] != 0
      return [number_words[digits[0]], "billion", eight_digits(num % 100_000_000)].join(" ")
    elsif digits[3] != 0
      return [number_words[digits[0]], "billion", seven_digits(num % 10_000_000)].join(" ")
    elsif digits[4] != 0
      return [number_words[digits[0]], "billion", six_digits(num % 1_000_000)].join(" ")
    elsif digits[5] != 0
      return [number_words[digits[0]], "billion", five_digits(num % 100_000)].join(" ")
    elsif digits[6] != 0
      return [number_words[digits[0]], "billion", four_digits(num % 10_000)].join(" ")
    elsif digits[7] != 0
      return [number_words[digits[0]], "billion", three_digits(num % 1_000)].join(" ")
    elsif digits[8] != 0
      return [number_words[digits[0]], "billion", double_digits(num % 100)].join(" ")
    else
      return [number_words[digits[0]], "billion", single_digit(num % 10)].join(" ")
    end
  end

  private
  def eleven_digits(num = nil)
    num = self unless num
    digits = num.to_s.split("").map { |x| x.to_i }
    first_two_digits = [digits[0], digits[1]].join.to_i

    if num % 10_000_000_000 == 0
      return [number_words[digits[0] * 10], "billion"].join(" ")
    elsif digits[1] != 0
      return [number_words[digits[0] * 10], ten_digits(num % 10_000_000_000)].join(" ")
    elsif digits[2] != 0
      return [number_words[first_two_digits], "billion", nine_digits(num % 1_000_000_000)].join(" ")
    elsif digits[3] != 0
      return [number_words[first_two_digits], "billion", eight_digits(num % 100_000_000)].join(" ")
    elsif digits[4] != 0
      return [number_words[first_two_digits], "billion", seven_digits(num % 10_000_000)].join(" ")
    elsif digits[5] != 0
      return [number_words[first_two_digits], "billion", six_digits(num % 1_000_000)].join(" ")
    elsif digits[6] != 0
      return [number_words[first_two_digits], "billion", five_digits(num % 100_000)].join(" ")
    elsif digits[7] != 0
      return [number_words[first_two_digits], "billion", four_digits(num % 10_000)].join(" ")
    elsif digits[8] != 0
      return [number_words[first_two_digits], "billion", three_digits(num % 1_000)].join(" ")
    elsif digits[9] != 0
      return [number_words[first_two_digits], "billion", double_digits(num % 100)].join(" ")
    else
      return [number_words[first_two_digits], "billion", single_digit(num % 10)].join(" ")
    end
  end

  private
  def twelve_digits(num = nil)
    num = self unless num
    digits = num.to_s.split("").map { |x| x.to_i }

    if num % 100_000_000_000 == 0
      return [number_words[digits[0]], "hundred billion"].join(" ")
    elsif digits[1] != 0
      return [number_words[digits[0]], "hundred", eleven_digits(num % 100_000_000_000)].join(" ")
    elsif digits[2] != 0
      return [number_words[digits[0]], "hundred", ten_digits(num % 10_000_000_000)].join(" ")
    elsif digits[3] != 0
      return [number_words[digits[0]], "hundred billion", nine_digits(num % 1_000_000_000)].join(" ")
    elsif digits[4] != 0
      return [number_words[digits[0]], "hundred billion", eight_digits(num % 100_000_000)].join(" ")
    elsif digits[5] != 0
      return [number_words[digits[0]], "hundred billion", seven_digits(num % 10_000_000)].join(" ")
    elsif digits[6] != 0
      return [number_words[digits[0]], "hundred billion", six_digits(num % 1_000_000)].join(" ")
    elsif digits[7] != 0
      return [number_words[digits[0]], "hundred billion", five_digits(num % 100_000)].join(" ")
    elsif digits[8] != 0
      return [number_words[digits[0]], "hundred billion", four_digits(num % 10_000)].join(" ")
    elsif digits[9] != 0
      return [number_words[digits[0]], "hundred billion", three_digits(num % 1_000)].join(" ")
    elsif digits[10] != 0
      return [number_words[digits[0]], "hundred billion", double_digits(num % 100)].join(" ")
    else
      return [number_words[digits[0]], "hundred billion", single_digit(num % 10)].join(" ")
    end
  end

  private
  def thirteen_digits(num = nil)
    num = self unless num
    digits = num.to_s.split("").map { |x| x.to_i }

    if num % 1_000_000_000_000 == 0
      return [number_words[digits[0]], "trillion"].join(" ")
    elsif digits[1] != 0
      return [number_words[digits[0]], "trillion", twelve_digits(num % 1_000_000_000_000)].join(" ")
    elsif digits[2] != 0
      return [number_words[digits[0]], "trillion", eleven_digits(num % 100_000_000_000)].join(" ")
    elsif digits[3] != 0
      return [number_words[digits[0]], "trillion", ten_digits(num % 10_000_000_000)].join(" ")
    elsif digits[4] != 0
      return [number_words[digits[0]], "trillion", nine_digits(num % 1_000_000_000)].join(" ")
    elsif digits[5] != 0
      return [number_words[digits[0]], "trillion", eight_digits(num % 100_000_000)].join(" ")
    elsif digits[6] != 0
      return [number_words[digits[0]], "trillion", seven_digits(num % 10_000_000)].join(" ")
    elsif digits[7] != 0
      return [number_words[digits[0]], "trillion", six_digits(num % 1_000_000)].join(" ")
    elsif digits[8] != 0
      return [number_words[digits[0]], "trillion", five_digits(num % 100_000)].join(" ")
    elsif digits[9] != 0
      return [number_words[digits[0]], "trillion", four_digits(num % 10_000)].join(" ")
    elsif digits[10] != 0
      return [number_words[digits[0]], "trillion", three_digits(num % 1_000)].join(" ")
    elsif digits[11] != 0
      return [number_words[digits[0]], "trillion", double_digits(num % 100)].join(" ")
    else
      return [number_words[digits[0]], "trillion", single_digit(num % 10)].join(" ")
    end

  end

  public
  def in_words
    digits_of_number = self.to_s.split("").map { |num| num.to_i }
    number_of_digits = digits_of_number.count

    case number_of_digits
    when 1
      single_digit
    when 2
      double_digits
    when 3
      three_digits
    when 4
      four_digits
    when 5
      five_digits
    when 6
      six_digits
    when 7
      seven_digits
    when 8
      eight_digits
    when 9
      nine_digits
    when 10
      ten_digits
    when 11
      eleven_digits
    when 12
      twelve_digits
    when 13
      thirteen_digits
    else
      return "Number not supported...yet"
    end
  end
end
